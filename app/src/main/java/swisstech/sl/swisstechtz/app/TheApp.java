package swisstech.sl.swisstechtz.app;

import android.app.Application;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.pushtorefresh.storio2.contentresolver.StorIOContentResolver;
import com.pushtorefresh.storio2.contentresolver.impl.DefaultStorIOContentResolver;

/**
 * Created by Aleksandr Pokhilko on 08.12.2017
 */

public class TheApp extends Application {

    private static StorIOContentResolver storIOContentResolver;

    @Override
    public void onCreate(){
        super.onCreate();

        Logger.addLogAdapter(new AndroidLogAdapter());

        if( storIOContentResolver == null ) {
            storIOContentResolver = DefaultStorIOContentResolver.builder()
                    .contentResolver(getContentResolver())
                    .build();
        }
    }

    public static StorIOContentResolver getResolver(){
        return storIOContentResolver;
    }

}