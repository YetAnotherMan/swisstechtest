package swisstech.sl.swisstechtz.presentation.model;

/**
 * Created by Aleksandr Pokhilko on 08.12.2017
 */

public class Photo {

    private String id;
    private String displayName;
    private String hash;
    private String size;

    public Photo(String id, String displayName, String hash, String size){
        this.id = id;
        this.displayName = displayName;
        this.hash = hash;
        this.size = size;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}