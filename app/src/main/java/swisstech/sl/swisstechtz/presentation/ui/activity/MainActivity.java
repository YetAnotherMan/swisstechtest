package swisstech.sl.swisstechtz.presentation.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.Collection;

import swisstech.sl.swisstechtz.R;
import swisstech.sl.swisstechtz.databinding.ActivityMainBinding;
import swisstech.sl.swisstechtz.presentation.model.Photo;
import swisstech.sl.swisstechtz.presentation.presenter.MainPresenter;
import swisstech.sl.swisstechtz.presentation.ui.adapter.PhotoAdapter;
import swisstech.sl.swisstechtz.presentation.view.MainView;


public class MainActivity extends MvpAppCompatActivity implements MainView {

    private ActivityMainBinding binding;
    private PhotoAdapter adapter;

    @InjectPresenter
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        initRecyclerView();

        binding.setHandler(view -> {
            if( adapter.getItemCount() > 0 ) {
                presenter.clearPhotos();
                adapter.clear();
            }

            presenter.showPhotosFromGallery(this);
        });
    }

    private void initRecyclerView() {
        adapter = new PhotoAdapter();

        GridLayoutManager manager = new GridLayoutManager(this, 3);

        RecyclerView rvPhotos = binding.rvPhotos;
        rvPhotos.setLayoutManager(manager);
        rvPhotos.setAdapter(adapter);
    }

    @Override
    public void showProgress() {
        binding.pbProgress.show();
    }

    @Override
    public void hideProgress() {
        binding.pbProgress.hide();
    }

    @Override
    public void showButtonEnabled(boolean isEnabled) {
        binding.btnShow.setEnabled(isEnabled);
    }

    @Override
    public void scrollListToEnd() {
        binding.rvPhotos.scrollToPosition(adapter.getItemCount() - 1);
    }

    @Override
    public void showPhoto(Photo photo) {
        adapter.addPhoto(photo);
    }

    @Override
    public void showPhotos(Collection<Photo> photos) {
        adapter.addPhotos(photos);
    }

    public interface ShowButtonListener {
        void onClick(View view);
    }
}