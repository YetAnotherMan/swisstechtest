package swisstech.sl.swisstechtz.presentation.presenter;

import android.Manifest;
import android.app.Activity;
import android.net.Uri;
import android.provider.MediaStore;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.orhanobut.logger.Logger;
import com.pushtorefresh.storio2.contentresolver.queries.Query;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.util.ArrayList;
import java.util.Arrays;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import swisstech.sl.swisstechtz.app.TheApp;
import swisstech.sl.swisstechtz.presentation.model.Photo;
import swisstech.sl.swisstechtz.presentation.view.MainView;
import swisstech.sl.swisstechtz.utils.HashUtils;

/**
 * Created by Aleksandr Pokhilko on 11.12.2017
 */
@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    private Uri photoSUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    private ArrayList<Photo> photosList = new ArrayList<>();
    private Subscription subscription;
    private Subscription thumbnailSubscription;

    @Override
    public void onFirstViewAttach(){
        super.onFirstViewAttach();
        getViewState().hideProgress();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        if (!subscription.isUnsubscribed())
            subscription.unsubscribe();

        if (!thumbnailSubscription.isUnsubscribed())
            thumbnailSubscription.unsubscribe();
    }

    public void clearPhotos(){
        photosList.clear();
    }

    public void showPhotosFromGallery(Activity activity){
        getViewState().showButtonEnabled(false);
        RxPermissions rxPermissions = new RxPermissions(activity);
        rxPermissions.request(Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) {
                        observePhotoChanges();

                        getViewState().showProgress();
                        loadThumbnails();
                    }
                });
    }

    private void observePhotoChanges() {
        thumbnailSubscription = TheApp.getResolver().observeChangesOfUri(photoSUri)
                .subscribe(changes -> {
                            getLastPhoto();
                        },
                        error -> Logger.d(error.getMessage()));
    }

    private void getLastPhoto() {
        final Query query = Query.builder()
                .uri(photoSUri)
                .build();

        TheApp.getResolver().get()
                .cursor()
                .withQuery(query)
                .prepare()
                .asRxObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cursor -> {
                    int idColumn = cursor.getColumnIndex(MediaStore.Images.ImageColumns._ID);
                    int displayNameColumn = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME);
                    int sizeColumn = cursor.getColumnIndex(MediaStore.Images.ImageColumns.SIZE);

                    cursor.moveToLast();

                    String id = cursor.getString(idColumn);
                    String displayName = cursor.getString(displayNameColumn);
                    String size = cursor.getString(sizeColumn);
                    String hash = HashUtils.hashFrom(displayName);

                    Photo photo = new Photo(id, displayName, hash, size);

                    getViewState().showPhoto(photo);
                    getViewState().scrollListToEnd();

                    Logger.d("added: %s  %s  %s %s", id, displayName, size, hash);

                    cursor.close();

                    thumbnailSubscription.unsubscribe();
                }, error -> Logger.e(error.getMessage()));
    }

    private void loadThumbnails() {
        final Query query = Query.builder()
                .uri(photoSUri)
                .build();

        subscription = TheApp.getResolver().get()
                .cursor()
                .withQuery(query)
                .prepare()
                .asRxObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cursor -> {
                    Logger.d("columns:" + Arrays.toString(cursor.getColumnNames()));

                    int idColumn = cursor.getColumnIndex(MediaStore.Images.ImageColumns._ID);
                    int displayNameColumn = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME);
                    int sizeColumn = cursor.getColumnIndex(MediaStore.Images.ImageColumns.SIZE);

                    while (cursor.moveToNext()) {
                        String id = cursor.getString(idColumn);
                        String displayName = cursor.getString(displayNameColumn);
                        String size = cursor.getString(sizeColumn);
                        String hash = HashUtils.hashFrom(displayName);

                        photosList.add(new Photo(id, displayName, hash, size));
                    }

                    cursor.close();

                    getViewState().showPhotos(photosList);
                    getViewState().hideProgress();
                    getViewState().showButtonEnabled(true);

                    subscription.unsubscribe();
                }, error -> Logger.e(error.getMessage()));
    }

}
