package swisstech.sl.swisstechtz.presentation.ui.adapter;

import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import swisstech.sl.swisstechtz.presentation.model.Photo;
import swisstech.sl.swisstechtz.databinding.ItemPhotoBinding;

/**
 * Created by Aleksandr Pokhilko on 08.12.2017
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {

    private List<Photo> photos = new ArrayList<>();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemPhotoBinding binding = ItemPhotoBinding.inflate(LayoutInflater.from(parent.getContext()));
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Photo photo = photos.get(position);
        holder.bind(photo);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public void addPhoto(Photo photo) {
        photos.add(photo);
        notifyItemRangeInserted(photos.size()-1, 1);
    }

    public void clear(){
        photos.clear();
        notifyDataSetChanged();
    }

    public void addPhotos(Collection<Photo> photos) {
        this.photos.addAll(photos);
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ItemPhotoBinding binding;

        ViewHolder(ItemPhotoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Photo photo) {
            binding.tvFileName.setText(photo.getDisplayName());
            binding.tvHash.setText(photo.getHash());

            long sizeInBytes = Long.parseLong(photo.getSize());
            long sizeInMB = sizeInBytes / 1024 / 1024;
            binding.tvSize.setText(sizeInMB + " MB");

            Uri uri = Uri.parse(MediaStore.Images.Media.EXTERNAL_CONTENT_URI + File.separator + photo.getId());
            Glide.with(binding.ivThumbnail.getContext()).load(uri).into(binding.ivThumbnail);
        }
    }
}