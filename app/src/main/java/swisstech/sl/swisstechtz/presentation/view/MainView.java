package swisstech.sl.swisstechtz.presentation.view;

import com.arellomobile.mvp.MvpView;

import java.util.Collection;

import swisstech.sl.swisstechtz.presentation.model.Photo;

/**
 * Created by Aleksandr Pokhilko on 11.12.2017
 */

public interface MainView extends MvpView {

    void showProgress();
    void hideProgress();
    void showButtonEnabled(boolean isEnabled);
    void scrollListToEnd();
    void showPhoto(Photo photo);
    void showPhotos(Collection<Photo> photos);

}
